#!/usr/bin/env python2.7
import pymysql.cursors
import re
import os
import sys
from flask_mysqldb import MySQL
from markupsafe import escape
import random
import json
import logging
logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p', filename='/var/log/chargesecure/flask_server.log')
logging.basicConfig(level=logging.warning)
import time
from flask import Flask, abort, request, jsonify, g, url_for
import requests
from flask_sqlalchemy import SQLAlchemy
from flask_httpauth import HTTPBasicAuth
import jwt
from werkzeug.security import generate_password_hash, check_password_hash

connection = pymysql.connect(host="localhost", user="", password="", database="")

# initialization
app = Flask(__name__)
app.config['SECRET_KEY'] = ''
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db.sqlite'
app.config['SQLALCHEMY_COMMIT_ON_TEARDOWN'] = True

# extensions
db = SQLAlchemy(app)
auth = HTTPBasicAuth()

app.config['MYSQL_HOST'] = ""
app.config['MYSQL_USER'] = ""
app.config['MYSQL_PASSWORD'] = ""
app.config['MYSQL_DB'] = ""
mysql = MySQL(app)


class User(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(32), index=True)
    password_hash = db.Column(db.String(128))

    def hash_password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

    def generate_auth_token(self, expires_in=600):
        return jwt.encode(
            {'id': self.id, 'exp': time.time() + expires_in},
            app.config['SECRET_KEY'], algorithm='HS256')

    @staticmethod
    def verify_auth_token(token):
        try:
            data = jwt.decode(token, app.config['SECRET_KEY'],
                              algorithms=['HS256'])
        except:
            return
        return User.query.get(data['id'])


@app.before_first_request
def create_tables():
    logging.info("First request, create tables")
    db.create_all()

@auth.verify_password
def verify_password(username_or_token, password):
    # first try to authenticate by token
    logging.info("Verifying auth token")
    user = User.verify_auth_token(username_or_token)
    if not user:
        # try to authenticate with username/password
        user = User.query.filter_by(username=username_or_token).first()
        if not user or not user.verify_password(password):
            return False
    g.user = user
    return True


@app.route('/api/users', methods=['POST'])
def new_user():
    username = request.json.get('username')
    password = request.json.get('password')
    if username is None or password is None:
        abort(400)    # missing arguments
    if User.query.filter_by(username=username).first() is not None:
        abort(400)    # existing user
    user = User(username=username)
    user.hash_password(password)
    db.session.add(user)
    db.session.commit()
    return (jsonify({'username': user.username}), 201,
            {'Location': url_for('get_user', id=user.id, _external=True)})


@app.route('/api/users/<int:id>')
def get_user(id):
    user = User.query.get(id)
    if not user:
        abort(400)
    return jsonify({'username': user.username})


@app.route('/api/token')
@auth.login_required
def get_auth_token():
    token = g.user.generate_auth_token(600)
    return jsonify({'token': token.decode('ascii'), 'duration': 600})

@app.route('/api/test')
@auth.login_required
def get_resource():
    return jsonify({'data': 'Hello, %s!' % g.user.username})

@app.route('/is_request/<call_id>')
def is_request(call_id):
    logging.info("Looking for existing request for call_id:%s", call_id)

    live_request = 0

    try:
        cursor = connection.cursor()
        sql = "select * from agi_requests where call_id = '%s'" % call_id
        logging.info("Query:%s", sql)
        logging.info("Executing query")
        connection.ping(reconnect=True)
        cursor.execute(sql)
        logging.info("Fetching result")
        result=cursor.fetchone()
        if result is not None:
            logging.info("result:%s", result)
            live_request = result[1]
            logging.info("live request:%s", live_request)
        else:
            logging.info("result was none:%s", result)
        
        logging.info("closing cursor")
        cursor.close()
        logging.info("cursor closed")
    except pymysql.Error as e:
        logging.warning("failed to run query to find live requests")
        logging.warning("could not close connection error pymysql %d: %s" %(e.args[0], e.args[1]))
    finally:
        cursor.close()

    if live_request == 0:
        logging.warning("live_request is 0")
        logging.warning(live_request)
        del live_request
        return str(0)
    else:
        logging.warning("live request is not zero, its...")
        logging.warning(live_request)
        temp_val = live_request
        del live_request
        return str(temp_val)
        

@app.route('/is_dtmf/<call_id>')
def is_dtmf(call_id):
    logging.warning("how many dtmf to capture for this call?")

    live_request = 0

    try:
        with connection.cursor() as cursor:
            # Create a new record
            sql = "select no_of_dtmf from agi_requests where call_id = '%s'" % call_id
            logging.warning(sql)
            connection.ping(reconnect=True)
            cursor.execute(sql)
            result=cursor.fetchone()
            live_request = result[0]
    except:
        logging.warning("failed to run query to find no_of_dtmf")

    logging.warning(live_request)

    if live_request == 0:
        return str(live_request)
    else:
        return str(live_request)

@app.route('/dact/<call_id>')
def delete_act(call_id):
    logging.warning("removing agi request record from db. call_id:%s", call_id)

    try:
        with connection.cursor() as cursor:
            # Create a new record
            sql = "delete from agi_requests where call_id ='%s'" % (call_id)
            connection.ping(reconnect=True)  
            cursor.execute(sql)
            # connection is not autocommit by default. So you must commit to save
            # your changes.
            connection.commit()
            return jsonify({'success': 'true'})
    except:
        logging.warning("failed to delete agi request")
        logging.warning(sql)
        return jsonify({'success': 'false'})




@app.route('/dagi/<call_id>')
def delete_call(call_id):
    logging.warning("removing agi call record from db...")
    logging.warning(call_id)

    try:
        with connection.cursor() as cursor:
            # Create a new record
            sql = "delete from agi_calls where call_id ='%s'" % (call_id)
            connection.ping(reconnect=True)  
            cursor.execute(sql)

            # connection is not autocommit by default. So you must commit to save
            # your changes.
            connection.commit()
    except:
        logging.warning("failed to delete agi calls")
        return jsonify({'success': 'false'})

    logging.warning("removing agi request record from db...")
    logging.warning(call_id)

    try:
        with connection.cursor() as cursor:
            # Create a new record
            sql = "delete from agi_requests where call_id ='%s'" % (call_id)
            connection.ping(reconnect=True)  
            cursor.execute(sql)

            # connection is not autocommit by default. So you must commit to save
            # your changes.
            connection.commit()
            return jsonify({'success': 'true'})
    except:
        logging.warning("failed to delete agi request")
        return jsonify({'success': 'false'})

    

@app.route('/user_map/')

@app.route('/api/test/post/<call_id>/<path:memberinterface>')
def test_agi(call_id, memberinterface):
    logging.warning("received data from a PBX")
    logging.warning("call_id:%s", call_id)
  
    logging.warning("is the request from a queue or a direct to endpoint call?")
    if (memberinterface.startswith("Local")) == True:
        username =  re.search(r'^Local\/(.*?)-(.*?)-(.*?)-(.*?)@.*$', memberinterface).group(4) # this method is used when we get the strange string from queue
        logging.warning("appears to be in a queue")
    else: 
        username = memberinterface
        logging.warning("it doesn't appear to be in a queue")
    logging.warning("username:"),
    logging.warning(username)


    logging.warning("does this user exist in mappings? only create entry in system if they exist...")
    user_exists = "0"

    try:
        with connection.cursor() as cursor:
            # Create a new record
            logging.info("looking for user:%s", username)
            sql = "SELECT sipalto_user_id from user_mappings where sipalto_user_id like '%%%s%%'" % (str(username))
            logging.warning(sql)
            connection.ping(reconnect=True)  
            cursor.execute(sql)
            result=cursor.fetchone()
            user_exists = result[0]
    except:
        logging.warning("failed to find user, query was ...")
        logging.warning(sql)

    if (user_exists != "0"):
        logging.warning("deleting any active requests because user exists:%s", user_exists)
        try:
            with connection.cursor() as cursor:
                # Create a new record
                sql = "delete from agi_requests where sipalto_user_id ='%s'" % (str(username))
                logging.warning(sql)
                connection.ping(reconnect=True)
                cursor.execute(sql)

                # connection is not autocommit by default. So you must commit to save
                # your changes.
                connection.commit()
        except:
            logging.warning("failed to delete agi calls")
            return jsonify({'success': 'false'})


    if (user_exists != "0"):
        logging.warning("deleting any active calls because user exists:%s", user_exists)
        try:
            with connection.cursor() as cursor:
                # Create a new record
                sql = "delete from agi_calls where sipalto_user_id ='%s'" % (str(username))
                logging.warning(sql)
                connection.ping(reconnect=True)
                cursor.execute(sql)

                # connection is not autocommit by default. So you must commit to save
                # your changes.
                connection.commit()
        except:
            logging.warning("failed to delete agi calls")
            return jsonify({'success': 'false'})


    if (user_exists != "0"):
        logging.warning("user exists:%s", user_exists)
        try:
            with connection.cursor() as cursor:
                # Create a new record
                logging.warning("creating a record in agi_calls, query is...")
                sql = "INSERT INTO agi_calls (uuid, call_id, sipalto_user_id) VALUES (uuid(), '%s', '%s')" % (str(call_id), str(username))
                logging.warning(sql)
                connection.ping(reconnect=True)  
                cursor.execute(sql)

                # connection is not autocommit by default. So you must commit to save
                # your changes.
                connection.commit()
        except:
            logging.warning("failed to insert into agi calls, query was ...")
            logging.warning(sql)
    else:
        logging.warning("user does not exist, dont enter into database")



#    try:
#        with connection.cursor() as cursor:
###            # Create a new record
#            logging.warning("creating a record in agi_calls, query is...")
#            sql = "INSERT INTO agi_calls (uuid, call_id, sipalto_user_id) VALUES (uuid(), '%s', '%s')" % (str(call_id), str(username))
#            logging.warning(sql)
#            cursor.execute(sql)#
#
#            # connection is not autocommit by default. So you must commit to save
#            # your changes.
#            connection.commit()
#    except:
#        logging.warning("failed to insert into agi calls, query was ...")
#        logging.warning(sql)


    #cursor = mysqldb.cursor()
    ##sql = "select count(*) from kamailio.dispatcher"
    #sql = "INSERT INTO agi_calls (uuid, call_id, user_id) VALUES (uuid(), '%s', '%s')" % (str(call_id), str(username))
    #logging.warning("query..."
    #print sql
    #cursor.execute(sql)
    
    #results = cursor.fetchall()
    #print str(results)
    
    return jsonify({'data': 'Hello, %s!' % call_id})

@app.route('/return_value/<dtmf>/<call_id>')
def return_value(dtmf, call_id):


    logging.warning("dtmf:%s", dtmf)
    logging.warning("call_id:%s", call_id)

    callback_url = "0"
    logging.warning("get callback url")
    try:
        with connection.cursor() as cursor:
            # Create a new record
            sql = "select callback_url from agi_requests where call_id = '%s'" % call_id
            logging.warning(sql)
            connection.ping(reconnect=True)  
            cursor.execute(sql)
            result=cursor.fetchone()
            callback_url = result[0]
    except:
        logging.warning("failed to run query to find no_of_dtmf")

    logging.warning("callback_url:%s", callback_url)

    request_id = "0" 
    logging.warning("get request_id")
    try:
        with connection.cursor() as cursor:
            # Create a new record
            sql = "select request_id from agi_requests where call_id = '%s'" % call_id
            logging.warning(sql)
            connection.ping(reconnect=True)  
            cursor.execute(sql)
            result=cursor.fetchone()
            request_id = result[0]
    except:
        logging.warning("failed to run query to find no_of_dtmf")

    logging.warning("request_id:%s", request_id)



    myobj = '{ "request_id":"%s", "dtmf_captured":"%s", "timed_out":false}' % (str(request_id), str(dtmf))
    logging.warning(myobj)

    logging.warning(":::sending data to this callback_url:%s", callback_url)
    r = requests.post(callback_url, data = myobj)
    logging.warning(":::finished sending, here's the respose...")
    logging.warning(r.text.encode("utf-8"))
    return "success"

@app.route('/capture/<user_id>/<no_of_dtmf>/<path:callback_url>')
@auth.login_required
def hello_name(callback_url, user_id, no_of_dtmf):
    request_id = random.randint(100000000000,999999999999)
    logging.warning("hello")
    logging.warning("received request to capture DTMF")
    logging.warning("callback_url:")
    logging.warning(callback_url)
    logging.warning("user_id:")
    logging.warning(user_id)
    logging.warning("no_of_dtmf:")
    logging.warning(no_of_dtmf)
    #r = requests.get('http://api.chargesecure.co.uk:80/return_value/%s/%s/%s' % (no_of_dtmf,request_id,callback_url))

    live_call = 0
    sipalto_user_id = 0
    live_requests = 0

    try:
        with connection.cursor() as cursor:
            # Create a new record
            logging.warning("is there an existing user mapping setup? using query ...")
            sql = "select sipalto_user_id from user_mappings where api_user_id = '%s'" % user_id
            logging.warning(sql)
            connection.ping(reconnect=True)  
            cursor.execute(sql)
            result=cursor.fetchone()
            sipalto_user_id = result[0]
    except:
            logging.warning("unable to find user mapping")

    try:
        with connection.cursor() as cursor:
            # Create a new record
            logging.warning("is there an active call for this user? using query ...")
            sql = "select * from agi_calls where sipalto_user_id in (%s)" % sipalto_user_id
            logging.warning(sql)
            connection.ping(reconnect=True)  
            cursor.execute(sql)
            logging.warning("doing fetch one...")
            result=cursor.fetchone()
            logging.warning("assigning result..")
            live_call = result[1]
    except:
        logging.warning("failed to run query to find live calls")

    logging.warning("live calls count:"), (live_call)


    if live_call == 0:
        if sipalto_user_id == 0:
            logging.warning("no live calls and no user in system")
            return jsonify({'callback_url':'%s' % (callback_url), 'user_id':'%s' % (user_id), 'no_of_dtmf':'%s' % (no_of_dtmf), 'request_id':'%s' % (request_id),'user_exists':'false','capturing_dtmf':'false','success':'false'})
        else:
            logging.warning("no live calls, but existing user")
            return jsonify({'callback_url':'%s' % (callback_url), 'user_id':'%s' % (user_id), 'no_of_dtmf':'%s' % (no_of_dtmf), 'request_id':'%s' % (request_id),'user_exists':'true','capturing_dtmf':'false','success':'false'})       
    else:
        call_id = live_call[1]
        logging.warning("there is a live call, so create action in DB")
        logging.warning("first delete any existing requests if there are any ...")

        try:
            with connection.cursor() as cursor:
                # Create a new record
                logging.warning("count number of requests for this user id")
                sql = "select count(*) from agi_requests where api_user_id = '%s'" % (user_id)
                logging.warning(sql)
                connection.ping(reconnect=True)  
                cursor.execute(sql)
                logging.warning("doing fetch one...")
                result=cursor.fetchone()
                logging.warning("assigning result..")
                logging.warning(result[0])
                live_requests = result[0]
        except:
            logging.warning("failed to run query to count number of agi_requests associated to this user")

        if live_requests >= 1:
            logging.warning("we have active request associated to this user, so delete it")
            try:
                with connection.cursor() as cursor:
                    # Create a new record
                    logging.warning("using query...")
                    sql = "delete from agi_requests where api_user_id ='%s'" % (user_id)
                    logging.warning(sql)
                    connection.ping(reconnect=True)  
                    cursor.execute(sql)
                    connection.commit()
                    logging.warning("finished execute and commit")
            except:
                logging.warning("failed to delete agi request")

            logging.warning("creating an action...")
            try:
                with connection.cursor() as cursor:
                    # Create a new record
                    logging.warning("using query...")
                    sql = "INSERT INTO agi_requests (uuid, request_id, api_user_id, sipalto_user_id, call_id, no_of_dtmf, callback_url) VALUES (uuid(), '%s', '%s', '%s', '%s', '%s', '%s')" % (str(request_id), str(user_id), str(sipalto_user_id), str(live_call), str(no_of_dtmf), str(callback_url))
                    logging.warning(sql)
                    connection.ping(reconnect=True)  
                    cursor.execute(sql)

                    connection.commit()
                    return jsonify({'callback_url':'%s' % (callback_url), 'user_id':'%s' % (user_id), 'no_of_dtmf':'%s' % (no_of_dtmf), 'request_id':'%s' % (request_id),'user_exists':'true','capturing_dtmf':'true','success':'true'})
            except:
                logging.warning("failed to insert into agi requests")

        else:
            logging.warning("no active request")
            try:
                logging.warning("creating an action...")
                with connection.cursor() as cursor:
                    # Create a new record
                    logging.warning("using query...")
                    sql = "INSERT INTO agi_requests (uuid, request_id, api_user_id, sipalto_user_id, call_id, no_of_dtmf, callback_url) VALUES (uuid(), '%s', '%s', '%s', '%s', '%s', '%s')" % (str(request_id), str(user_id), str(sipalto_user_id), str(live_call), str(no_of_dtmf), str(callback_url))
                    logging.warning(sql)
                    connection.ping(reconnect=True)  
                    cursor.execute(sql)

                    connection.commit()
                    return jsonify({'callback_url':'%s' % (callback_url), 'user_id':'%s' % (user_id), 'no_of_dtmf':'%s' % (no_of_dtmf), 'request_id':'%s' % (request_id),'user_exists':'true','capturing_dtmf':'true','success':'true'})
            except:
                logging.warning("failed to insert into agi requests")



if __name__ == '__main__':
    #if not os.path.exists('db.sqlite'):
    #    db.create_all()
    app.run(debug=True)